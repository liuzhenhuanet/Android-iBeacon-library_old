package com.hua.androidibeaconlib;
/**
 * 带计数功能的ibeacon类，该计数可以用来确定最近多少次没有被扫描到了，如果计数比较大，
 * 则可能关联的ibeacon可能已经不在可搜索范围内了。
 * @author liuzhenhua
 *
 */
public class IBeaconWithCount extends IBeacon {

	public IBeaconWithCount(String address, String uuid, int major, int minor,
			int rssi, int tx_power) {
		super(address, uuid, major, minor, rssi, tx_power);
	}
	
	public IBeaconWithCount(IBeacon ibeacon){
		super(ibeacon.getAddress(),
				ibeacon.getUuid(),
				ibeacon.getMajor(),
				ibeacon.getMinor(),
				ibeacon.getRssi(),
				ibeacon.getTx_power()
				);
	}
	
	private int count=0;  //该iBeacon连续多少次没有被扫描到过了，如果该值比较大，则说明可能
	                    //该iBeacon可能不在可搜寻的范围内了

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
	

}
